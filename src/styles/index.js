import styled from 'styled-components'

export const Button = styled.button`
  background: transparent;
  color: palevioletred;
  border: 2px solid palevioletred;
  transition: .5s;
  font-family: 'Nunito', sans-serif;
  font-weight: 700;
  outline: none;
  cursor: pointer;
  display: inline-block;
  margin: ${props => props.delup ? '0 3px' : '0'}
  &:hover{
    background: palevioletred;
    color: white;
  }
`;
export const Input = styled.input`
  padding: 0.5em;
  margin: 0.5em;
  color: palevioletred;
  background: papayawhip;
  outline: none;
  border: 2px solid palevioletred!important;
  border: none;
  border-radius: 3px;
  height: 8px;
  display: inline-block;
  &::-webkit-input-placeholder {
    color: palevioletred;
  }
  &::-moz-placeholder{
    color: palevioletred;
  }
`;
export const Span = styled.span`
  display: inline-block;
  width: 455px;
  text-align: left
  color: ${props => props.fin ? 'green' : 'inherit'}
`;