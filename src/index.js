import React from 'react'; //Подключение из node_modules
import ReactDOM from 'react-dom'; //Подключение из node_modules

import App from './components/App' //импортируем компонент app

ReactDOM.render(
    <App />,//компонент, который нужно отрендерить
    document.getElementById('root')//обертка, в которую будет положен результат
);