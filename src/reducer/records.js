//Редьюсер это функция,которая принимает на вход состояние нашего стора
//а на выход возвращает новое состояние
import {DELETE_RECORD, ADD_RECORD, UPDATE_RECORD, UPDATE_STATUS} from '../constans'

export const items = []

export default (recordsState = items, action) => {
    const {type} = action

    switch (type){
        case ADD_RECORD:
            //const newState = recordsState;
            //newState.push(action.payload);
            return [...recordsState, action.payload];
        case DELETE_RECORD:
            return recordsState.filter((item, i) => i !== action.payload.index);
        case UPDATE_RECORD:
            return recordsState.map((item, i) => i === action.payload.index ? recordsState[i] = action.payload.todovalue : recordsState[i])
        case UPDATE_STATUS:
            return recordsState.map((item, i) => i === action.payload.index ? !recordsState[i].includes('(finished)') ? recordsState[i] = recordsState[i].concat('(finished)') : recordsState[i].replace('(finished)','') : recordsState[i])
        default : break
    }
    return recordsState
}