import {combineReducers} from 'redux'
import recordsReducer from './records'

export default combineReducers({
    recordsReducer //es6
})