import {ADD_RECORD, DELETE_RECORD, UPDATE_RECORD, UPDATE_STATUS} from '../constans'

export function addRecord(payload) {
    return{
        type: ADD_RECORD,
        payload,
    }
}

export function deleteRecord(index) {
    return{
        type: DELETE_RECORD,
        payload: { index }
    }
}

export function updateRecord(index, todovalue) {
    return{
        type: UPDATE_RECORD,
        payload: { index, todovalue }
    }
}

export function updateStatus(index) {
    return{
        type: UPDATE_STATUS,
        payload: { index }
    }
}