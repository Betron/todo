import {createStore} from 'redux'
import reducer from '../reducer'
//Метод создающий store, вызываем один раз при инициализации
//Единственный обязательный аргумент для стор это редьюсер
const  store = createStore(reducer,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

//dev only служит для удобного взаимодействия
window.store = store

export default store