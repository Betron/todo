import React from 'react'
import {connect} from 'react-redux'
import {deleteRecord, updateRecord, updateStatus} from "../AC/index";
import {Button,Span} from '../styles'

const ulStyle = {
    textAlign: 'center',
    listStyle: 'none',
};

function ToDoItem(props) {
    const toDoElements = props.records.map((element, index) => <li key={index}><Span fin={element.includes('(finished)') ? true : false} onClick={() => props.updateStatus(index)}>{index+1}. {element}</Span> <Button delup onClick={() => props.deleteRecord(index)}>Delete Me</Button><Button delup onClick={() => props.updateRecord(index, props.todovalue)}>Update Me</Button></li>)
    return(
        <ul style={ulStyle}>
            {toDoElements}
        </ul>
    )
}

export default connect((state) => ({
    records: state.recordsReducer
}), {deleteRecord, updateRecord, updateStatus})(ToDoItem)
