import React from 'react'; //Подключение из node_modules
import ToDoForm from './ToDoForm'
import store from '../store'
import {Provider} from 'react-redux'
import {Button, Input} from '../styles'

const h1Style = {
    textAlign: 'center',
};

export default class App extends React.Component{

    state = {
        log : '',
        pas : '',
        flag: 0,
    }

    render(){
        return(
            <Provider store = {store}>
                <div>
                    <h1 style={h1Style}>ToDoList</h1>
                    {this.state.flag === 0 &&
                        <div style={h1Style}>
                            <span>(Username & Password - admin)</span><br />
                            <span>Username:</span>
                            <Input type="text" placeholder="Example: Alex" value={this.state.log} onChange={this.handleUser}/><br />
                            <span>Password:</span>
                            <Input type="password" value={this.state.pas} onChange={this.handlePas}/><br />
                            <Button onClick={this.logIn}>Log In</Button>
                        </div>
                    }
                    {this.state.flag === 1 &&
                    <ToDoForm logOut = {this.logOut}/>}
                </div>
            </Provider>
        )
    }

    handleUser = (ev) => {
        this.setState({
            log: ev.target.value
        })
    }

    handlePas = (ev) => {
        this.setState({
            pas: ev.target.value
        })
    }

    logIn = () =>{
        if(this.state.log === 'admin' && this.state.pas === 'admin'){
            this.setState({
                flag: 1
            })
        }
        else {
            alert('Sorry, try again!')
            this.setState({
                log: '',
                pas: ''
            })
        }
    }

    logOut = () =>{
        this.setState({
            flag: 0,
            log: '',
            pas: ''
        })
    }

}
