import React from 'react'
import ToDoItemList from './ToDoItemList'
//import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {addRecord} from '../AC'
import {Button, Input} from '../styles'

const inputFormStyle = {
    textAlign: 'center',
}

class ToDoForm extends React.Component{
    state = {
        todoval: '',
        newtask: 0,
    }

    render(){
        return(
            <div style={inputFormStyle}>

                <span>If you need leave: </span><Button onClick={this.props.logOut}>Log Out</Button><br /><br /><br />
                <Input type="text" placeholder="Example: Take a shower" value={this.state.todoval} onChange={this.handleToDoVal}/>
                <Button onClick={this.newToDo}>Add</Button>
                <h2>You're ToDoList have {this.props.records.length} record(s)</h2>
                <span>If you wanna update a status task just click!</span>
                <ToDoItemList todovalue = {this.state.todoval}/>
                <Button onClick={this.returnAllTasks}>Return all tasks in a task list</Button>
            </div>
        )
    }
    handleToDoVal = (ev) => {
        //Ограничиваем ввод до 20 символов
        if(ev.target.value.length > 20) return
        this.setState({
            todoval: ev.target.value
        })
    }
    newToDo = () =>{
        this.props.addRecord(this.state.todoval);
        this.setState({
            todoval: ''
        })
    }
    returnAllTasks = () => {
        alert('All tasks: ' + this.props.records)
    }
}

export default connect((state) => ({
    records: state.recordsReducer
}), {addRecord})(ToDoForm)
